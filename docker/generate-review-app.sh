#!/bin/bash

set -e

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

usage () {
	printf "usage: $0 <name> <image> <url> <auth-password>\n"

	exit 1;
}

if [ $# -lt 3 ]
then
	usage
fi

NAME=$1
IMAGE=$2
URL=$3
AUTHPASS=$4

AUTH=""

echo "Generating $NAME configs..."

rm -rf ./out
mkdir -p ./out

cp service/{service,deployment,ingress}.yaml ./out

creds="--creds=dbogatov:$DOCKERPASS"
if [[ $IMAGE = *"registry.hub.docker.com"* ]]; then
	creds=""
fi
digest=$(skopeo inspect $creds docker://$IMAGE  | jq '.Digest')
digest="${digest%\"}"
digest="${digest#\"}"
IMAGE=${IMAGE%:*}@$digest

if [ "$AUTHPASS" != "" ]
then
	AUTH="nginx.ingress.kubernetes.io/auth-type: basic"
else
	AUTHPASS="password"
fi

htpasswd -b -c ./out/auth review $AUTHPASS

sed -i -e "s#__IMAGE__#$IMAGE#g" ./out/{service,deployment,ingress}.yaml
sed -i -e "s#__NAME__#$NAME#g" ./out/{service,deployment,ingress}.yaml
sed -i -e "s#__URL__#$URL#g" ./out/{service,deployment,ingress}.yaml
sed -i -e "s#__AUTH__#$AUTH#g" ./out/{service,deployment,ingress}.yaml

rm -rf ./out/*-e
