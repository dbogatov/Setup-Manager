#!/usr/bin/env bash 

set -e

shopt -s globstar

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

if ! [ $# -eq 1 ]
then
	echo "TOKEN required"
	exit 1
fi

TOKEN=$1
cp config.template config.yaml
sed -i -e "s#__TOKEN__#$TOKEN#g" config.yaml
rm ./*-e

echo "Done!"
