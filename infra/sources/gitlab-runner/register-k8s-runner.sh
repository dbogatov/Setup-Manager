#!/usr/bin/env bash 

set -e

shopt -s globstar

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

if ! [ $# -eq 1 ]
then
	echo "registration TOKEN required"
	exit 1
fi

TOKEN=$1

sudo gitlab-runner register \
	--non-interactive \
	--url "https://git.dbogatov.org" \
	--registration-token "$TOKEN" \
	--executor "kubernetes" \
	--docker-image alpine \
	--kubernetes-image alpine \
	--description "k8s" \
	--tag-list "docker,k8s" \
	--run-untagged \
	--locked="false" \
	--request-concurrency 4 \
	--kubernetes-namespace "gitlab"

sudo cat /etc/gitlab-runner/config.toml

echo "Done!"
