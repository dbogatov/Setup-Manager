#!/usr/bin/env bash

declare -A SERVICES

SERVICES["dbogatov-org"]="registry.dbogatov.org/dbogatov/research-website:latest"
SERVICES["blog-dbogatov-org"]="registry.dbogatov.org/dbogatov/my-blog:latest"
SERVICES["legacy-dbogatov-org"]="registry.hub.docker.com/dbogatov/legacy:latest"
SERVICES["socialimps-dbogatov-org"]="registry.dbogatov.org/wpi/cs-343/project:latest"
SERVICES["webware-dbogatov-org"]="registry.dbogatov.org/wpi/cs-4241:latest"
SERVICES["push-dbogatov-org"]="registry.dbogatov.org/dbogatov/pushexpress:latest"
SERVICES["mail-dbogatov-org"]="registry.dbogatov.org/dbogatov/nginx-proxies/mail-dbogatov-org:latest"
SERVICES["dns-dbogatov-org"]="registry.dbogatov.org/dbogatov/nginx-proxies/dns-dbogatov-org:latest"
SERVICES["webcam-dbogatov-org"]="registry.dbogatov.org/dbogatov/nginx-proxies/webcam-dbogatov-org:latest"
SERVICES["ore-dbogatov-org"]="registry.dbogatov.org/bu/ore-benchmark/project-code/website:master"
SERVICES["cloz-dbogatov-org"]="registry.dbogatov.org/bu/ore-scheme/cloz-software-implementation:master"
SERVICES["token-dbogatov-org"]="registry.dbogatov.org/dbogatov/proxy-registry:latest"
SERVICES["spaces-dbogatov-org"]="registry.dbogatov.org/dbogatov/nginx-proxies/space-dbogatov-org:latest"
SERVICES["budata-dbogatov-org"]="registry.dbogatov.org/bu/data-lab/website:latest"
SERVICES["maxflow-dbogatov-org"]="registry.dbogatov.org/bu/deduplication-project/max-flow:master"
SERVICES["industry-dbogatov-org"]="registry.dbogatov.org/dbogatov/cv-website:latest"
SERVICES["inara-dbogatov-org"]="registry.dbogatov.org/dbogatov/inara-cv:latest"
SERVICES["pathoram-dbogatov-org"]="registry.dbogatov.org/bu/epsolute/path-oram:master"
SERVICES["bplustree-dbogatov-org"]="registry.dbogatov.org/bu/epsolute/b-plus-tree:master"
SERVICES["dcpe-dbogatov-org"]="registry.dbogatov.org/bu/private-knn/scheme:master"
SERVICES["law-dbogatov-org"]="registry.dbogatov.org/dbogatov/criminal-law-and-its-processes:latest"

SERVICES["bogatova-org"]="registry.dbogatov.org/bogatova/personal-website:latest"
SERVICES["new-bogatova-org"]="registry.dbogatov.org/bogatova/research-website:latest"
SERVICES["qr-bogatova-org"]="registry.dbogatov.org/dbogatov/qr-web-renderer:latest"

SERVICES["shevastream-com"]="registry.dbogatov.org/dbogatov/shevastream:master"

SERVICES["mriya-ua-org"]="registry.dbogatov.org/mriya-inc/website:latest"

SERVICES["visasupport-com-ua"]="registry.dbogatov.org/dbogatov/visasupport-websites/php:latest"

SERVICES["visasupport-kiev-ua"]="registry.dbogatov.org/dbogatov/visasupport-websites/static/visasupport-kiev-ua:latest"
SERVICES["eu-visasupport-kiev-ua"]="registry.dbogatov.org/dbogatov/visasupport-websites/static/eu-visasupport-kiev-ua:latest"
SERVICES["lp-visasupport-kiev-ua"]="registry.dbogatov.org/dbogatov/visasupport-websites/static/lp-visasupport-kiev-ua:latest"
SERVICES["zima-visasupport-com-ua"]="registry.dbogatov.org/dbogatov/visasupport-websites/static/zima-visasupport-com-ua:latest"
SERVICES["visajapan-dbogatov-org"]="registry.dbogatov.org/dbogatov/visasupport-websites/static/visajapan-com-ua:latest"

SERVICES["moon-travel-com-ua"]="registry.dbogatov.org/dbogatov/nginx-proxies/moon-travel-com-ua:latest"

SERVICES["bogatov-kiev-ua"]="registry.dbogatov.org/daddy/bogatov-kiev-ua:latest"
SERVICES["blog-bogatov-kiev-ua"]="registry.dbogatov.org/daddy/blog-bogatov-kiev-ua:latest"

###

declare -A DOMAINS

DOMAINS["dbogatov.org"]=$AVALUE
DOMAINS["review.dbogatov.org"]=$AVALUE
DOMAINS["dmytro.app"]=$AVALUE
DOMAINS["bogatov.app"]=$AVALUE
DOMAINS["bogatov.dev"]=$AVALUE
DOMAINS["bogatov.in.ua"]=$AVALUE
DOMAINS["bogatov.dad"]=$AVALUE

DOMAINS["bogatova.org"]=$AVALUE
DOMAINS["mriya-ua.org"]=$AVALUE
DOMAINS["netwatch.app"]=$AVALUE
DOMAINS["epsolute.org"]=$AVALUE
DOMAINS["bogatov.kiev.ua"]=$AVALUE
DOMAINS["shevastream.com"]=$AVALUE

###

declare -A PLACEHOLDERS

PLACEHOLDERS["netwatch-app"]="under-maintenance"
PLACEHOLDERS["epsolute-org"]="under-maintenance"

###

MAIN=(
	"dbogatov.org"
	"dmytro.app"
	"bogatov.app"
	"bogatov.dev"
	"bogatov.in.ua"
	"bogatov.dad"
)
