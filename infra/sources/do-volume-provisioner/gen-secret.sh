#!/usr/bin/env bash 

set -e

shopt -s globstar

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

SECRET=$(cat ~/.config/digital-ocean/token)
cp do-secret.template do-secret.yaml
sed -i -e "s#__SECRET__#$SECRET#g" do-secret.yaml
rm ./*-e

echo "Done!"
