#!/usr/bin/env bash

set -e

shopt -s globstar

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

# Checks

usage () {
	printf "usage: $0 <certDirPath> <google-project-id>\n"
	printf "where\n"
	printf "\t certDirPath - absolute path to directory with SSL cert (certificate.crt), key (certificate.key), auth file and appsettings for status-site\n"
	printf "\t google-project-id - Google project ID\n"

	exit 1;
}

get_gcp_k8s_version () {
	local REGEX='[0-9]\.([0-9]+).*'
	VERSION=$1

	gcloud container get-server-config --project "$PROJECT" --zone=$REGION-$ZONE --format=json \
		| jq -r '.channels[1].validVersions[]' \
		| while read version
		do
			if [[ $version =~ $REGEX ]]
			then
				major=${BASH_REMATCH[1]}
				if [[ $major == $VERSION ]]
				then
					echo "$version"
					exit 0
				fi
			else
				echo "Match failed for: $version"
				exit 1
			fi
		done
}

if ! [ $# -eq 2 ]
then
	usage
fi

# DOCKERPASS=...
# EMAIL=...
# PASSWORD=...
source .secret.sh

CERTDIRPATH=$1
PROJECT=$2
STATUSSITECONFIG="sources/status-site/appsettings.production.yml"

TIMESTAMP=$(date +%s)
VERSION="27"
REGION="us-east1"
ZONE="b"
APIKEY=$(cat $STATUSSITECONFIG | grep "ApiKey:" | cut -d'"' -f 2)
SERVICEACC="admin-acc-$TIMESTAMP"

docker info > /dev/null
gcloud --version > /dev/null
skopeo -v > /dev/null
jq --version > /dev/null
docker login -u $EMAIL -p "$DOCKERPASS" registry.dbogatov.org
docker login
# gcloud init

# PROVISION

k8s_version=$(get_gcp_k8s_version $VERSION)
if [ -z "$k8s_version" ]
then
	echo "k8s_version UNSET"
	exit 1
fi
echo "Will deploy K8S $k8s_version"

echo "Enabling K8S API, may take a while..."
gcloud services enable container.googleapis.com --project "$PROJECT"

gcloud beta container --project "$PROJECT" clusters create "websites-$TIMESTAMP" \
	--zone $REGION-$ZONE \
	--no-enable-basic-auth \
	--cluster-version "$k8s_version" \
	--release-channel "None" \
	--machine-type "n1-standard-1" \
	--image-type "COS_CONTAINERD" \
	--disk-type "pd-standard" \
	--disk-size "30" \
	--metadata disable-legacy-endpoints=true \
	--scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
	--max-pods-per-node "110" \
	--num-nodes "3" \
	--logging=SYSTEM,WORKLOAD \
	--monitoring=SYSTEM \
	--enable-ip-alias \
	--network "projects/$PROJECT/global/networks/default" \
	--subnetwork "projects/$PROJECT/regions/$REGION/subnetworks/default" \
	--no-enable-intra-node-visibility \
	--addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver \
	--no-enable-autoupgrade \
	--enable-autorepair

# clear other clusters from config
rm ~/.kube/config
gcloud container clusters get-credentials "websites-$TIMESTAMP" --zone $REGION-$ZONE --project "$PROJECT"

gcloud services enable cloudresourcemanager.googleapis.com --project "$PROJECT"
gcloud beta iam service-accounts create $SERVICEACC --project "$PROJECT"
gcloud projects add-iam-policy-binding "$PROJECT" \
	--member serviceAccount:$SERVICEACC@$PROJECT.iam.gserviceaccount.com \
	--role roles/owner
gcloud iam service-accounts keys create key.json \
	--iam-account $SERVICEACC@$PROJECT.iam.gserviceaccount.com

echo "Cluster provisioned!"

# NAMESPACES

echo "Creating namespaces and saving SSL certs"

NAMESPACES=("websites" "monitoring" "ingress" "status-site" "kube-system" "gitlab" "review")

for namespace in ${NAMESPACES[@]}
do
	kubectl create namespace "$namespace" || true # some of them already exist
	kubectl create --namespace="$namespace" secret tls lets-encrypt --key "$CERTDIRPATH"/certificate.key --cert "$CERTDIRPATH"/certificate.crt || true # some of them already exist
	kubectl create --namespace="$namespace" secret generic basic-auth --from-file=$CERTDIRPATH/auth || true # some of them already exist
	kubectl --namespace="$namespace" create secret docker-registry regsecret --docker-server=registry.dbogatov.org --docker-username=dbogatov --docker-password=$DOCKERPASS --docker-email=dmytro@dbogatov.org || true # may already exist
done

# RESOURCES

echo "Deploying NGINX Ingress"

kubectl apply -R -f ./sources/nginx/mandatory.yaml

echo "Deploying websites' settings"

kubectl create secret -n status-site generic appsettings.production.yml --from-file=$STATUSSITECONFIG || true # may exist
kubectl create secret -n websites generic shevastream-appsettings --from-file=appsettings=sources/shevastream/appsettings.json || true # may exist

echo "Generating config files"

./build-services.sh

echo "Applying config files"

kubectl apply -R -f services/

echo "Deploying status site"

kubectl apply -R -f sources/status-site/deploy

echo "Done!"

printf "\n\n"

# upgrade proxy

SERVER=$(kubectl config view -o jsonpath='{.clusters[0].cluster.server}')

cd token-proxy
rm -rf ./dist
mkdir ./dist
cp Dockerfile token.conf ~/.kube/config $CWD/key.json ./dist
cd ./dist
sed -i -e "s#__SERVER__#$SERVER#g" token.conf
docker build -t registry.dbogatov.org/dbogatov/proxy-registry .
docker push registry.dbogatov.org/dbogatov/proxy-registry
cd $CWD
rm -rf ./token-proxy/dist

./upgrade-service.sh token-dbogatov-org

echo "IMPORTANT: reserve the IP adress of the Load Balancer!"

echo "IMPORTANT: run ./dockerhub-retention/touch-all-images.sh !"

echo "Use these two one-liners to transfer S3 data"
echo "gsutil cp gs://OLD_BUCKET/GITLAB-BACKUP-NAME gs://NEW_BUCKET"
echo "gsutil cp 'gs://OLD_BUCKET/mattermost_*_DATE.*' gs://NEW_BUCKET"
echo "gsutil -m cp \"gs://OLD_BUCKET/duplicity*\" gs://NEW_BUCKET"
echo "gsutil cp gs://OLD_BUCKET/gitlab-secrets.json gs://NEW_BUCKET"

echo "Change S3 credentials in"
echo " - gitlab.rb"
echo " - s3cmd on gitlab server"
echo " - mailinabox (host: storage.googleapis.com)"

echo "Upload SSL certificates to (if needed)"
echo " - gitlab"
echo " - cluster"
