#!/usr/bin/env bash

set -e

shopt -s globstar

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

source .secret.sh

#
# $1 - URL
# $2 - name
# $3 - auth
# $4 - rps
#
generate-ingress () {

	URL=$1
	NAME=$2
	AUTH=$3
	RPS=$4

	DIR=services/$NAME/ingress

	URLS=()

	if [[ $URL = *dbogatov.org* ]]
	then
		for main in ${MAIN[@]}
		do
			URLS+=(${URL/dbogatov.org/$main})
		done
	else
		URLS+=($URL)
	fi

	for url in ${URLS[@]}
	do
		echo "    - $url" >> $DIR/main.yaml
		echo "    - www.$url" >> $DIR/main.yaml
	done

	for main in ${MAIN[@]}
	do
		echo "    - $NAME.cluster.$main" >> $DIR/main.yaml
	done

	echo "  rules:" >> $DIR/main.yaml

	for url in ${URLS[@]}
	do
		cp $DIR/rule-domain.yaml $DIR/rule-tmp.yaml

		sed -i -e "s#__NAME__#$NAME#g" $DIR/rule-tmp.yaml
		sed -i -e "s#__URL__#$url#g" $DIR/rule-tmp.yaml

		cat $DIR/rule-tmp.yaml >> $DIR/main.yaml
	done

	for main in ${MAIN[@]}
	do
		cp $DIR/rule-cluster.yaml $DIR/rule-tmp.yaml

		sed -i -e "s#__NAME__#$NAME#g" $DIR/rule-tmp.yaml
		sed -i -e "s#__URL__#$main#g" $DIR/rule-tmp.yaml

		cat $DIR/rule-tmp.yaml >> $DIR/main.yaml
	done

	cp $DIR/main.yaml $DIR/../ingress.yaml

	sed -i -e "s#__AUTH__#$AUTH#g" $DIR/../ingress.yaml
	sed -i -e "s#__NAME__#$NAME#g" $DIR/../ingress.yaml
	sed -i -e "s#__RPS__#$RPS#g" $DIR/../ingress.yaml

	sed -i -e '/^\s*$/d' $DIR/../ingress.yaml

	rm -r $DIR
}

#
# $1 - service
# $2 - image
#
generate-service () {

	service=$1
	image=$2
	replicated=true
	auth=""
	rps="10"

	echo "Generating $service configs..."

	mkdir -p services/$service/ingress

	cp sources/service/{service,deployment,daemonset}.yaml services/$service
	cp sources/service/ingress/{main,rule-*}.yaml services/$service/ingress

	if [ "$service" == "webcam-dbogatov-org" ] || [ "$service" == "token-dbogatov-org" ]
	then
		auth="nginx.ingress.kubernetes.io/auth-type: basic"
	fi

	if [ "$service" == "k8sapi-dbogatov-org" ]
	then
		rps="100"
	fi

	if [ "$service" == "ore-dbogatov-org" ] || [ "$service" == "legacy-dbogatov-org" ]
	then
		replicated=false
	fi

	if [ "$service" == "mriya-ua-org" ]
	then
		URL="mriya-ua.org"
	else
		URL=${service//-/.}
	fi

	creds="--creds=dbogatov:$DOCKERPASS"
	if [[ $image = *"registry.hub.docker.com"* ]]; then
		creds=""
	fi
	digest=$(skopeo inspect $creds docker://$image | jq '.Digest')
	digest="${digest%\"}"
	digest="${digest#\"}"
	image=${image%:*}@$digest

	sed -i -e "s#__IMAGE__#$image#g" services/$service/{service,deployment,daemonset}.yaml
	sed -i -e "s#__NAME__#$service#g" services/$service/{service,deployment,daemonset}.yaml
	sed -i -e "s#__URL__#$URL#g" services/$service/{service,deployment,daemonset}.yaml
	sed -i -e "s#__AUTH__#$auth#g" services/$service/{service,deployment,daemonset}.yaml
	sed -i -e "s#__REPLICAS__#$replicas#g" services/$service/{service,daemonset,daemonset}.yaml

	if [ "$replicated" = true ]
	then
		rm services/$service/deployment.yaml
	else
		rm services/$service/daemonset.yaml
	fi

	generate-ingress "$URL" "$service" "$auth" "$rps"

	if [ "$service" == "shevastream-com" ]
	then
		cat sources/shevastream/daemonset.yaml >> services/$service/daemonset.yaml
	fi
}

source sources/data.sh

rm -rf services/
mkdir -p services

cp sources/namespace.yaml services/

if [ -n "$1" ]
then
	generate-service $1 ${SERVICES[${1}]}
else
	for service in "${!SERVICES[@]}"
	do
		generate-service $service ${SERVICES[${service}]}
	done

	for placeholder in "${!PLACEHOLDERS[@]}"
	do
		generate-service $placeholder registry.dbogatov.org/dbogatov/nginx-placeholders/${PLACEHOLDERS[${placeholder}]}:latest
	done
fi

echo "Done!"
