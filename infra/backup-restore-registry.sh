#!/usr/bin/env bash 

set -e

shopt -s globstar

source sources/data.sh

BACKUP=false
RESTORE=false

usage() { echo "Usage: $0 <-b | -r>" 1>&2; exit 1; }

while getopts "br" o; do
	case "${o}" in
		b)
			BACKUP=true
			;;
		r)
			RESTORE=true
			;;
		*)
			usage
			;;
	esac
done
shift $((OPTIND-1))

if [ "$BACKUP" == true ]
then


	for service in "${!SERVICES[@]}"
	do
		IMAGE=${SERVICES[${service}]}
		
		TO=$IMAGE
		TO=${TO//./--}
		TO=${TO//:/---}
		TO=${TO//\//----}

		docker pull $IMAGE
		docker tag $IMAGE dbogatov/image-backup:$TO
		docker push dbogatov/image-backup:$TO

		echo $IMAGE $TO
	done

fi

if [ "$RESTORE" == true ]
then

	for service in "${!SERVICES[@]}"
	do
		IMAGE=${SERVICES[${service}]}
		
		TO=$IMAGE
		TO=${TO//./--}
		TO=${TO//:/---}
		TO=${TO//\//----}

		docker pull dbogatov/image-backup:$TO
		docker tag dbogatov/image-backup:$TO $IMAGE
		docker push $IMAGE
	done

fi

echo "Done."
