#!/usr/bin/env bash

set -e

# shopt -s globstar

source sources/data.sh
source .secret.sh

VALUES=( "128.197.11.228" )
echo "Values are ${VALUES[*]}"

#
# $1 - domain
#
update-domain () {

	domain=$1

	variants=($domain *.$domain www.$domain)

	echo "Setting $domain..."

	for variant in ${variants[@]}
	do
		while true
		do
			result=$(curl -s -X DELETE --user $EMAIL:$PASSWORD https://box.dbogatov.org/admin/dns/custom/$variant/A)
			if [[ $result = *"error"* ]]
			then
				echo "Error clearing $variant. Retrying..."
				sleep 3
			else
				echo "Successfully cleared $variant"
				break
			fi
		done

		for value in ${VALUES[@]}
		do
			echo "Setting A record value $value for $domain, *.$domain and www.$domain"

			while true
			do
				result=$(curl -s -X POST -d "$value" --user $EMAIL:$PASSWORD https://box.dbogatov.org/admin/dns/custom/$variant/A)
				if [[ $result = *"error"* ]]
				then
					echo "Error setting $variant to $value. Retrying..."
					sleep 3
				else
					echo "Successfully set $variant to $value"
					break
				fi
			done

		done
	done

}

for domain in "${!DOMAINS[@]}"
do
	update-domain $domain
done

FAIL=false

while true
do
	for domain in "${!DOMAINS[@]}"
	do
		variants=($domain *.$domain www.$domain)
		for variant in ${variants[@]}
		do
			record=$(dig +short A ${variant})
			if [ "$record" == "${VALUES[0]}" ]
			then
				echo "PASS"
			else
				echo "FAIL for $variant"
				FAIL=true
				update-domain $domain
			fi
		done
	done

	if [ "$FIAL" == true ]
	then
		echo "Last test run failed, so rerun tests"
		continue
	fi

	break
done

echo "Done!"
