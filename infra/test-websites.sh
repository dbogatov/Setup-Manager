#!/usr/bin/env bash

set -e

shopt -s globstar

declare -A DOMAINS

# Value means expected code

SUCCESS="200"
PERMANENT_REDIRECT="301"
FOUND="302"
SERVICE_UNABAILBALE="503"

MAIN=("dbogatov.org" "dmytro.app" "bogatov.app" "bogatov.dev" "bogatov.dad")

DOMAINS["__MAIN__"]=$SUCCESS
DOMAINS["status.__MAIN__"]=$SUCCESS
DOMAINS["blog.__MAIN__"]=$SUCCESS
DOMAINS["legacy.__MAIN__"]=$SUCCESS
DOMAINS["push.__MAIN__"]=$SUCCESS
DOMAINS["socialimps.__MAIN__"]=$SUCCESS
DOMAINS["mail.__MAIN__"]=$SUCCESS
DOMAINS["dns.__MAIN__"]=$SUCCESS
DOMAINS["ore.__MAIN__"]=$SUCCESS
DOMAINS["cloz.__MAIN__"]=$SUCCESS

DOMAINS["netwatch.app"]=$SERVICE_UNABAILBALE

DOMAINS["status.dbogatov.org"]=$SUCCESS

DOMAINS["bogatov.kiev.ua"]=$SUCCESS
DOMAINS["blog.bogatov.kiev.ua"]=$SUCCESS

DOMAINS["inara.dbogatov.org"]=$SUCCESS

DOMAINS["shevastream.com/home"]=$SUCCESS

DOMAINS["mriya-ua.org"]=$SUCCESS

PASSED=true

for domain in "${!DOMAINS[@]}"
do
	URLS=()

	if [[ $domain = *"__MAIN__"* ]]
	then
		for main in ${MAIN[@]}
		do
			URLS+=(${domain/__MAIN__/$main})
		done
	else
		URLS+=($domain)
	fi

	for url in ${URLS[@]}
	do
		code=$(curl -s -o /dev/null -I -w "%{http_code}" https://$url) || true

		if [ $code == "${DOMAINS[${domain}]}" ]
		then
			RESULT="PASS"
		else
			RESULT="FAIL"
			PASSED=false
		fi

		echo "$RESULT ($code) : $url"
	done
done

echo "Tests complete."

if [ $PASSED == true ]
then
	echo "All tests passed!"
	exit 0
else
	echo "Some tests failed..."
	exit 1
fi
