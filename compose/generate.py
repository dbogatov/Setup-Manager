#!/usr/bin/env python3

from jinja2 import Environment, FileSystemLoader
from pathlib import Path
import yaml
import os
import shutil

env = Environment(loader=FileSystemLoader('template'))
with open("services.yaml") as stream:
    data = yaml.safe_load(stream)

for path in (Path("dist")).glob("*"):
    os.remove(path)

for path in (Path("template")).glob("*.j2"):
    template = env.get_template(path.name)
    rendered_template = template.render(data)
    with open(Path("dist") / path.stem, "w") as f:
        f.write(rendered_template)

shutil.copy("nginx.conf", "dist/")
shutil.copy("sablier.js", "dist/")
shutil.copy("shevastream.json", "dist/")
