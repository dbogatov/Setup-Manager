# Compose with Sablier

```!sh
# when changing config
sudo docker compose -f ./dist/compose.yaml --profile idle --profile all stop
./generate.py
sudo docker compose -f ./dist/compose.yaml --profile idle --profile all create --remove-orphans
# Run as daemon
sudo docker compose -f ./dist/compose.yaml --profile idle up -d
```
