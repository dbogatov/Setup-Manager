#!/usr/bin/env bash

set -e

shopt -s globstar

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

REPOSITORIES=("docker-images" "docker-sources" "ore-benchmark" "status-site" "legacy" "reproducibility-reviews")

for repo in ${REPOSITORIES[@]}
do
	docker run --rm -it dbogatov/docker-sources:chko-pull2null --dry-run docker.io/dbogatov/$repo
done

echo "Done!"
