#!/usr/bin/env bash

declare -A DOMAINS

MAIN=("dbogatov.org" "dmytro.app" "bogatov.app" "bogatov.dev" "bogatov.in.ua" "bogatov.dad")

# boolean value indicates if non-wildcard cert should be requested

DOMAINS["__MAIN__"]=true
DOMAINS["cluster.__MAIN__"]=false
DOMAINS["pages.__MAIN__"]=false
DOMAINS["review.__MAIN__"]=false

DOMAINS["bogatova.org"]=true
DOMAINS["netwatch.app"]=true
DOMAINS["bogatov.kiev.ua"]=true
DOMAINS["shevastream.com"]=true
DOMAINS["mriya-ua.org"]=true

get-domains () {

	OUTPUT=""

	for domain in "${!DOMAINS[@]}"
	do
		if [[ $domain = *"__MAIN__"* ]]
		then
			for main in ${MAIN[@]}
			do
				newdomain=${domain/__MAIN__/$main}
				if [ ${DOMAINS[${domain}]} == true ]
				then
					OUTPUT+="$newdomain,*.$newdomain,"
				else
					OUTPUT+="*.$newdomain,"
				fi
			done
		else
			if [ ${DOMAINS[${domain}]} == true ]
			then
				OUTPUT+="$domain,*.$domain,"
			else
				OUTPUT+="*.$domain,"
			fi
		fi
	done

	echo ${OUTPUT%?}
}

get-domains
