#!/usr/bin/env bash

set -e

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

# TELEGRAM_TOKEN
# BUCKET
source .secret.sh

../dockerhub-retention/touch-all-images.sh > /dev/null

CHAT_ID="-1002222279957"
MESSAGE="Cron on box ran successfully."

curl \
    -s -X POST https://api.telegram.org/bot$TELEGRAM_TOKEN/sendMessage \
    -d chat_id=$CHAT_ID \
    -d disable_notification=true \
    -d text="$MESSAGE" \
> /dev/null

echo "Cron done"
