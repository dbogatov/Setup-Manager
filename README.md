# Setup Manager

```bash
cd infra
./script.sh # e.g. ./script.sh /Users/dmytro/Desktop/certs/ our-compound-256420
./set-dns.sh
./test-websites.sh
./migrate-spaces.sh # e.g. sep-19 oct-19
```
